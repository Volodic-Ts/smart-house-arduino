#include <DHT.h>

#if defined(ESP8266)
/* ESP8266 Dependencies */
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#elif defined(ESP32)
/* ESP32 Dependencies */
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <SPIFFS.h>
#endif
#include <ESPDash.h>

DHT dht(4, DHT11);

const char* ssid = "netis_CDFE3D"; // SSID netis_CDFE3D
const char* password = "30061999vova"; // Password 30061999vova

AsyncWebServer server(80);

int temperatureC;
double temperatureF;
int brightness;
int humidity;

void setup() {
  pinMode(34, INPUT);
  dht.begin();

  Serial.begin(115200);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.printf("WiFi Failed!\n");
    return;
  }
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());

  if(!SPIFFS.begin()){
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/index.html");
  });
  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/style.css", "text/css");
  });
  server.on("/brightness-ic.png", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/brightness-ic.png", "image/png");
  });
  server.on("/humidity-ic.png", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/humidity-ic.png", "image/png");
  });
  server.on("/oxygen-ic.png", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/oxygen-ic.png", "image/png");
  });
  server.on("/person-ic.png", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/person-ic.png", "image/png");
  });
  server.on("/temperature-ic.png", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/temperature-ic.png", "image/png");
  });
  server.on("/brightness", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", readBrightness().c_str());
  });
  server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", readDHTTemperature().c_str());
  });
  server.on("/humidity", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", readDHTHumidity().c_str());
  });

  server.begin();
}

void loop() {
  temperatureC = dht.readTemperature();
  temperatureF = temperatureC * 1.8 + 32;
  humidity = dht.readHumidity();
  brightness = analogRead(34);

}

String readBrightness() {
  brightness = analogRead(34);
  String bright = String(brightness); 

  return bright;
}

String readDHTTemperature() {
  temperatureC = dht.readTemperature();
  String temperature = String(temperatureC);

  return temperature;
}

String readDHTHumidity() {
  humidity = dht.readHumidity();
  String hum = String(humidity);

  return hum;
}

void printInfoFromSensors() {
  temperatureC = dht.readTemperature();
  temperatureF = temperatureC * 1.8 + 32;
  humidity = dht.readHumidity();
  brightness = analogRead(34);

  Serial.print("Temperature: ");
  Serial.print(temperatureC);
  Serial.print(" °C\n");

  Serial.print("Temperature: ");
  Serial.print(temperatureF);
  Serial.print(" °F\n");

  Serial.print("Humidity: ");
  Serial.print(humidity);
  Serial.print(" %\n");

  Serial.print("Brightness: ");
  Serial.print(brightness);
  Serial.println("\n");
}
